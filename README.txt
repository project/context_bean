CONTEXT BEAN
---------------------
This module introduces a bean type that can
display any number of other beans based on
existing contexts. The user must first create
a context and then create beans of any type.
They can then create a Context Bean that allows
for conditional view modes/outputting other beans
from within the context bean's markup.
